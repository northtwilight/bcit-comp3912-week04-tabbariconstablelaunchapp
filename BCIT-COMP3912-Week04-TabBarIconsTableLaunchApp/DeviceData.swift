//
//  DeviceData.swift
//  BCIT-COMP3912-Week04-TabBarIconsTableLaunchApp
//
//  Created by Massimo Savino on 2016-10-10.
//  Copyright © 2016 Massimo Savino. All rights reserved.
//

import Foundation

let coffeeData = [
    CoffeeMaker(name: "Bonavita", model: "Coffee Maker BV1900TS", rating: 1.44),
    CoffeeMaker(name: "Bonavita", model: "8-Cup Digital Coffee Brewer BV1900TD", rating: 5.00),
    CoffeeMaker(name: "Brazen Plus", model: "Customizable Temperature Control Brew System", rating: 3.75),
    CoffeeMaker(name: "Bunn", model: "Phase Brew 8 Cup Coffee Brewer", rating: 4.67),
    CoffeeMaker(name: "KitchenAid", model: "Coffee Maker KCM0802", rating: 2.45),
    CoffeeMaker(name: "KitchenAid", model: "Pour Over Coffee Brewer KCM0801OB", rating: 4.93),
    CoffeeMaker(name: "Lance Larkin", model: "BE 112 Brew Express", rating: 2.66),
    CoffeeMaker(name: "OXO On", model: "9-Cup Coffee Maker", rating: 2.25),
    CoffeeMaker(name: "OXO On", model: "12-Cup Coffee Maker", rating: 4.50),
    CoffeeMaker(name: "Technivorm", model: "Moccamaster", rating: 4.00),
    CoffeeMaker(name: "Wilfa", model: "Precision Coffee Maker", rating: 3.50)
]

let washingMachineData = [
    
    WashingMachine(name: "Amana", model: "NTW4750YQ", rating: 4.75),
    WashingMachine(name: "Bosch", model: "WAS20160UC", rating: 2.44),
    WashingMachine(name: "Electrolux", model: "EIFLS55IIW", rating: 3.43),
    WashingMachine(name: "Electrolux", model: "EIFLS55IMB", rating: 2.37),
    WashingMachine(name: "Frigidaire", model: "FFFS5115PW", rating: 3.34),
    WashingMachine(name: "GE", model: "GFWR4800FWW", rating: 4.65),
    WashingMachine(name: "GE", model: "GFWR4805FRR", rating: 1.33),
    WashingMachine(name: "Kenmore", model: "31522", rating: 4.53),
    WashingMachine(name: "Kenmore", model: "31412", rating: 3.35),
    WashingMachine(name: "LG", model: "F1495BDA", rating: 4.35),
    WashingMachine(name: "LG", model: "WT5680HWA", rating: 4.44),
    WashingMachine(name: "Maytag", model: "MHWE301YW", rating: 4.55),
    WashingMachine(name: "Samsung", model: "WF331ANW", rating: 4.56),
    WashingMachine(name: "Samsung", model: "WF56H9100AW", rating: 2.34),
    WashingMachine(name: "Samsung", model: "WW9000", rating: 3.45),
    WashingMachine(name: "Whirlpool", model: "WFW95HEDC", rating: 3.23)
]

let vacuumCleanerData = [
    
    VacuumCleaner(name: "Bissell", model: "CleanView Vacuum Cleaner", rating:  4.66),
    VacuumCleaner(name: "Dirt Devil", model: "Pro Express Upright Vacuum", rating: 2.59),
    VacuumCleaner(name: "Dyson", model: "360 Eye", rating: 3.43),
    VacuumCleaner(name: "Dyson", model: "Cinetic Big Ball Animal+Allergy Vacuum", rating: 4.32),
    VacuumCleaner(name: "Dyson", model: "V8 Absolute Cord-Free Stick Vacuum", rating: 3.37),
    VacuumCleaner(name: "Electrolux", model: "Ergorapido Power", rating: 4.99),
    VacuumCleaner(name: "Eureka", model: "Brushroll Cleaner with SuctionSeal AS3401A", rating: 4.55),
    VacuumCleaner(name: "Eureka", model: "SuctionSeal Pet S1104A Vacuum Cleaner", rating: 4.44),
    VacuumCleaner(name: "Hoover", model: "Platinum Collection Line Cordless Stick Vacuum", rating: 1.11),
    VacuumCleaner(name: "Hoover", model: "WindTunnel 2 Rewind Bag", rating: 3.57),
    VacuumCleaner(name: "Neato", model: "Botvac D85 Robot Vacuum", rating: 3.44),
    VacuumCleaner(name: "Neato", model: "XV Signature Pro", rating: 2.22),
    VacuumCleaner(name: "Oreck", model: "Touch Bagless Vacuum", rating: 3.33),
    VacuumCleaner(name: "Shark", model: "Rocket 2-in-1 Stick Vacuum", rating:  4.95),
    VacuumCleaner(name: "Shark", model: "Rotator Pro Lift-Away", rating: 3.21),
]


/** 
 COFFEE MACHINES
 
 Technivorm Moccamaster
 Brazen Plus Customizable Temperature Control Brew System
 KitchenAid Coffee Maker KCM0802
 KitchenAid Pour Over Coffee Brewer KCM0801OB
 Bonavita Coffee Maker BV1900TS
 Bonavita 8-Cup Digital Coffee Brewer BV1900TD
 OXO On 9-Cup Coffee Maker
 OXO On 12-Cup Coffee Maker
 Wilfa Precision Coffee Maker
 Lance Larkin BE 112 Brew Express
 Bunn Phase Brew 8 Cup Coffee Brewer
 
 
 WASHING MACHINES
 
 LG F1495BDA
 Samsung WW9000
 Samsung WF56H9100AW
 Whirlpool WFW95HEDC
 Electrolux EIFLS55IIW
 GE GFWR4800FWW
 GE GFWR4805FRR
 Maytag MHWE301YW
 Bosch WAS20160UC
 Samsung WF331ANW
 LG WT5680HWA
 Electrolux EIFLS55IMB
 Frigidaire FFFS5115PW
 Amana NTW4750YQ
 Kenmore 31522
 Kenmore 31412
 
 
 VACUUM CLEANERS
 
 Oreck Touch Bagless Vacuum
 Neato XV Signature Pro
 Hoover Platinum Collection Line Cordless Stick Vacuum
 Shark Rotator Pro Lift-Away
 Eureka SuctionSeal Pet S1104A Vacuum Cleaner
 Electrolux Ergorapido Power
 Dyson 360 Eye
 Neato Botvac D85 Robot Vacuum
 Eureka Brushroll Cleaner with SuctionSeal AS3401A
 Bissell CleanView Vacuum Cleaner
 Shark Rocket 2-in-1 Stick Vacuum
 Dyson V8 Absolute Cord-Free Stick Vacuum
 Dyson Cinetic Big Ball Animal+Allergy Vacuum
 Hoover WindTunnel 2 Rewind Bag
 Dirt Devil Pro Express Upright Vacuum

 
 
 */
