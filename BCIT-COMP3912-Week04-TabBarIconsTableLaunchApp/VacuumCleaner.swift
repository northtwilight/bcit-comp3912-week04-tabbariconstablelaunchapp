//
//  VacuumCleaner.swift
//  BCIT-COMP3912-Week04-TabBarIconsTableLaunchApp
//
//  Created by Massimo Savino on 2016-10-16.
//  Copyright © 2016 Massimo Savino. All rights reserved.
//

import Foundation

struct VacuumCleaner {
    
    var name: String?
    var model: String?
    var rating: Double?
    
    init(name: String?, model: String?, rating: Double?) {
        
        self.name = name
        self.model = model
        self.rating = rating
    }
}
