
//  CoffeeMakerTableViewController.swift
//  BCIT-COMP3912-Week04-TabBarIconsTableLaunchApp
//
//  Created by Massimo Savino on 2016-10-08.
//  Copyright © 2016 Massimo Savino. All rights reserved.
//

import UIKit

class CoffeeMakerTableViewController: UITableViewController {
    
    // MARK: Properties
    
    var coffeeMakers = coffeeData
    
    
    
    // MARK: Lifecycle

    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = Constants.Tabs.coffeeMakers
    }

    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return coffeeMakers.count
    }
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Tableviews.identifier, for: indexPath) as! CoffeeMakerTableViewCell
        let row = coffeeMakers[indexPath.row]
        
        cell.brandLabel.text = row.name
        cell.modelLabel.text = row.model
        cell.ratingLabel.text = String(describing: row.rating!)
        
        return cell
    }
}
