//
//  WashingMachineTableViewCell.swift
//  BCIT-COMP3912-Week04-TabBarIconsTableLaunchApp
//
//  Created by Massimo Savino on 2016-10-21.
//  Copyright © 2016 Massimo Savino. All rights reserved.
//

import UIKit

class WashingMachineTableViewCell: UITableViewCell {

    @IBOutlet weak var brandLabel: UILabel!
    
    @IBOutlet weak var modelLabel: UILabel!
    
    @IBOutlet weak var ratingLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
