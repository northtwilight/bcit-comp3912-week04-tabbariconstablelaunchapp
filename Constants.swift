//
//  Constants.swift
//  BCIT-COMP3912-Week04-TabBarIconsTableLaunchApp
//
//  Created by Massimo Savino on 2016-10-10.
//  Copyright © 2016 Massimo Savino. All rights reserved.
//

import Foundation

struct Constants {
    
    struct Tabs {
        
        static let coffeeMakers = "Coffee Makers"
        static let washingMachines = "Washing Machines"
        static let vacuumCleaners = "Vacuum Cleaners"
    }
    
    struct Tableviews {
        
        static let identifier = "Cell"
    }
    
    struct Errors {
        
        static let missingTableViewCell = "Missing table view cell"
    }
    
    struct CoffeeMaker {
               
        static let BonavitaBrand = "Bonavita"
        static let BrazenPlusBrand = "Brazen Plus"
        static let BunnBrand = "Bunn"
        static let KitchenAidBrand = "KitchenAid"
        static let LanceLarkinBrand = "Lance Larkin"
        static let OXOOnBrand = "OXO On"
        static let TechnivormBrand = "Technivorm"
        static let WilfaBrand = "Wilfa"
    }
    
    struct WashingMachine {
        
        static let AmanaBrand = "Amana"
        static let BoschBrand = "Bosch"
        static let ElectroluxBrand = "Electrolux"
        static let FrigidaireBrand = "Frigidaire"
        static let GEBrand = "GE"
        static let KenmoreBrand = "Kenmore"
        static let LGBrand = "LG"
        static let MaytagBrand = "Maytag"
        static let SamsungBrand = "Samsung"
        static let WhirlpoolBrand = "Whirlpool"
    }
    
    struct VacuumCleaner {
        
        static let BissellBrand = "Bissell"
        static let DirtDevilBrand = "Dirt Devil"
        static let DysonBrand = "Dyson"
        static let ElectroluxBrand = "Electrolux"
        static let EurekaBrand = "Eureka"
        static let HooverBrand = "Hoover"
        static let NeatoBrand = "Neato"
        static let OreckBrand = "Oreck"
        static let SharkBrand = "Shark"
    }
}
